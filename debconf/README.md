
debconf depends on Perl CGI/Gtk3/Net::LDAP which are rarely used, drag a lot of extra deps, and are not needed on our servers.

Also some packages are missing in EL8 like perl-LDAP:
  https://bugzilla.redhat.com/show_bug.cgi?id=1663063
So to avoid packaging so many packages, it's better to downgrade these deps to Suggests.


# Introduction

postfix-mta-sts-resolver provides a lookup daemon and command linei
query utility for MTA-STS policies (RFC 8461).  The daemon provides TLS
client policy to Postfix via socketmap.

# Reason for Packaging

This package is missing on Fedora and CentOS and we need it for our infra.

Review: soon


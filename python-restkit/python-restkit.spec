%global pypi_name restkit

Name:           python-%{pypi_name}
Version:        4.2.2
Release:        13%{?dist}
Summary:        Restkit is an HTTP resource kit for Python
License:        MIT
URL:            https://benoitc.github.com/restkit/
Source0:        https://pypi.python.org/packages/source/r/restkit/restkit-%{version}.tar.gz
Patch0:         restkit-disable-tests-needing-internet-connection.patch
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python2-setuptools
BuildRequires:  python2-nose
BuildRequires:  python2-socketpool >= 0.5.3
BuildRequires:  python2-http-parser >= 0.8.3
BuildRequires:  python2-webob

%global _description\
Restkit is a full HTTP client using pure socket calls and its own\
HTTP parser. It's not based on httplib or urllib2.

%description %_description

%package -n python2-%{pypi_name}
Summary: %summary
Requires:  python2-socketpool >= 0.5.3
Requires:  python2-http-parser >= 0.8.3
Requires:  python2-webob
%{?python_provide:%python_provide python2-%{pypi_name}}

%description -n python2-%{pypi_name} %_description

%prep
%setup -q -n %{pypi_name}-%{version}
# remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%patch0 -p0

%build
%{__python2} setup.py build


%install
%{__python2} setup.py install -O1 --skip-build --root %{buildroot} --install-data %{_docdir}
# remove zero-length file
rm %{buildroot}%{_docdir}/%{pypi_name}/TODO.txt
# strip shebang from non-executable file
sed -i '1d' %{buildroot}%{python2_sitelib}/%{pypi_name}/contrib/webob_api.py


%check
nosetests --exclude="009-test-oauth_filter"

%files -n python2-%{pypi_name}
%doc %{_docdir}/%{pypi_name}/
%{_bindir}/restcli
%{python2_sitelib}/restkit*
# exclude the test files that get into %%{python_sitelib}
%exclude %{python2_sitelib}/tests

%changelog
* Sat Feb 02 2019 Fedora Release Engineering <releng@fedoraproject.org> - 4.2.2-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Wed Jul 25 2018 Pierre-Yves Chibon <pingou@pingoured.fr> - 4.2.2-12
- Use the py2 version of the macros

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 4.2.2-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 4.2.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sat Jan 27 2018 Iryna Shcherbina <ishcherb@redhat.com> - 4.2.2-9
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Sat Aug 19 2017 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 4.2.2-8
- Python 2 binary package renamed to python2-restkit
  See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.2.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.2.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2.2-5
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 4.2.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Sep 26 2013 Bohuslav Kabrda <bkabrda@redhat.com> - 4.2.2-1
- Update to 4.2.2.

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Mon Nov 05 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 4.2.0-1
- Update to 4.2.0.

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.1.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Mar 27 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 4.1.2-2
- Remove IPython dependency (it is only optional).
- Stripped the shebang from non-executable file.

* Thu Mar 22 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 4.1.2-1
- Initial package.


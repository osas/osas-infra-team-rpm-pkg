# Introduction

needrestart checks which daemons need to be restarted after library upgrades. It is inspired by checkrestart from the debian-goodies package.

You can find more information on this site: https://github.com/liske/needrestart

# Reason for Packaging

This package is missing on Fedora and CentOS.

There is a script in the `yum-utils` package called `needrestarting`,
unfortunately it is only able to list services to restart and you're
left doing the rest of the job manually. Also needrestart is aware of
problematic services (systemd, dbus…) and provide a way to handle them.
There are some interesting features like container support which may be
of use in the future.


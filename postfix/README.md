Bump to >=3.4 to get SNI support needed by recent postfix-mta-sts-resolver

Rebuild to get all binary packages available.
See https://bugzilla.redhat.com/show_bug.cgi?id=1745321

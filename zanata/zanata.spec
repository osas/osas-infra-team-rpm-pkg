# Make this rpm compatible with older RHEL versions
%global _source_filedigest_algorithm 1
%global _binary_filedigest_algorithm 1
%global _binary_payload w9.gzdio
%global _source_payload w9.gzdio
%global java_ver_min   1.8.0
%global context_root /
%global zanata_home /var/lib/zanata

%define  _webappsdir %{_datadir}/webapps

Name: zanata
Version: 4.6.2
Release: 2
License: LGPL
Summary: Zanata translation tool
Group: Development/Libraries/Java
URL: http://zanata.org/

Source0: gwt-editor-4.6.2.war.md5
Source1: gwt-editor-4.6.2.war.sha1
Source2: gwt-editor-4.6.2.war
Source3: zanata-war-4.6.2.war.md5
Source4: zanata-war-4.6.2.war.sha1
Source5: zanata-war-4.6.2.war

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: java-1.8.0-openjdk-devel >= %{java_ver_min}
Requires: java >= 1:%{java_ver_min}

Requires: eap7-wildfly >= 7.1.1


%description
Artifacts for zanata

%prep

%build

%install
rm -rf %{buildroot}
/usr/bin/install -d %{buildroot}%{_webappsdir}
/usr/bin/install -d %{buildroot}%{_webappsdir}/%{name}
mkdir -p %{buildroot}%{zanata_home} %{buildroot}%{zanata_home}/index %{buildroot}%{zanata_home}/stats

/usr/bin/install -m 644 %{SOURCE0} %{buildroot}%{_webappsdir}/%{name}
/usr/bin/install -m 644 %{SOURCE1} %{buildroot}%{_webappsdir}/%{name}
mkdir %{buildroot}%{_webappsdir}/%{name}/gwt-editor-4.6.2.war
unzip -d %{buildroot}%{_webappsdir}/%{name}/gwt-editor-4.6.2.war %{SOURCE2}
/usr/bin/install -m 644 %{SOURCE3} %{buildroot}%{_webappsdir}/%{name}
/usr/bin/install -m 644 %{SOURCE4} %{buildroot}%{_webappsdir}/%{name}
mkdir %{buildroot}%{_webappsdir}/%{name}/zanata-war-4.6.2.war
unzip -d %{buildroot}%{_webappsdir}/%{name}/zanata-war-4.6.2.war %{SOURCE5}

cd %{buildroot}%{_webappsdir}/%{name}
ln -s zanata-war-*.war zanata.war
cd -

%clean
rm -rf %{buildroot}

%post
sed -i -e 's|<!-- _context_root_ -->|<context-root>%{context_root}</context-root>|' %{_webappsdir}/zanata/zanata.war/WEB-INF/jboss-web.xml

%files
%{_webappsdir}/%{name}
%attr(-, jboss, jboss) %{_webappsdir}/%{name}
%attr(-, jboss, jboss) /var/lib/%{name}

%changelog
* Thu Aug 03 2019 Marc Dequènes (Duck) <duck@redhat.com> - 4.6.2-2
- Remove useless sources, saving 53MB

* Mon Aug 20 2018 Ding-Yi Chen <dchen@redhat.com> - 4.6.2-1
- Upstream update to 4.6.2

* Tue Aug 14 2018 Ding-Yi Chen <dchen@redhat.com> - 4.6.1-1
- Upstream update to 4.6.1

* Fri Jul 27 2018 Ding-Yi Chen <dchen@redhat.com> - 4.6.0-1
- Upstream update to 4.6.0

* Sat Jun 23 2018 Ding-Yi Chen <dchen@redhat.com> - 4.5.0-1
- Upstream update to 4.5.0

* Thu May 10 2018 Ding-Yi Chen <dchen@redhat.com> - 4.4.5-1
- Upstream update to 4.4.5

* Thu Apr 26 2018 Ding-Yi Chen <dchen@redhat.com> - 4.4.4-1
- Upstream update to 4.4.4

* Thu Feb 07 2018 Ding-Yi Chen <dchen@redhat.com> - 4.4.3-1
- Upstream update to 4.4.3

* Thu Feb 01 2018 Ding-Yi Chen <dchen@redhat.com> - 4.4.1-1
- Upstream update to 4.4.1

* Tue Jan 30 2018 Ding-Yi Chen <dchen@redhat.com> - 4.4.0-1
- Upstream update to 4.4.0

* Fri Dec 15 2017 Ding-Yi Chen <dchen@redhat.com> - 4.3.3-1
- Upstream update to 4.3.3

* Fri Nov 24 2017 Ding-Yi Chen <dchen@redhat.com> - 4.3.2-1
- Upstream update to 4.3.2

* Mon Nov 06 2017 Ding-Yi Chen <dchen@redhat.com> - 4.3.1-1
- Upstream update to 4.3.1

* Mon Oct 08 2017 Ding-Yi Chen <dchen@redhat.com> - 4.3.0-1
- Upstream update to 4.3.0

* Wed Aug 30 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.4-1
- Upstream update to 4.2.4

* Fri Jun 09 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.1-2
- Set the java_ver_min to 1.8.0
  The actual version should be controled by ansible.

* Thu Jun 01 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.1-1
- Upstream update to 4.2.1

* Fri May 25 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.0-1
- Upstream update to 4.2.0

* Wed May 23 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.0_rc_5-1
- Upstream update to 4.2.0-rc-5

* Mon May 22 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.0_rc_4-1
- Upstream update to 4.2.0-rc-4

* Tue May 16 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.0_rc_3-2
- BuildRequires java-1.8.0-openjdk-devel, java-devel does not seem to work

* Tue May 16 2017 Ding-Yi Chen <dchen@redhat.com> - 4.2.0_rc_3-1
- Upstream update to 4.2.0-rc-3

* Sat Feb 25 2017 Ding-Yi Chen <dchen@redhat.com> - 4.1.1-2
- Correct bogus date

* Sat Feb 25 2017 Ding-Yi Chen <dchen@redhat.com> - 4.1.1-1
- Upsteam update to 4.1.1

* Sat Feb 11 2017 Ding-Yi Chen <dchen@redhat.com> - 4.1.0-1
- Upsteam update to 4.1.0

* Thu Dec 22 2016 Ding-Yi Chen <dchen@redhat.com> - 4.0.0-1
- Upsteam update to 4.0.0

* Fri Dec 09 2016 Ding-Yi Chen <dchen@redhat.com> - 4.0.0_rc_2-1
- Upsteam update to 4.0.0-rc-2

* Tue Nov 29 2016 Ding-Yi Chen <dchen@redhat.com> - 4.0.0_rc_1-1
- Upstream update to 4.0.0-rc-1
- Discard unneeded files.

* Fri Nov 18 2016 Ding-Yi Chen <dchen@redhat.com> - 4.0.0_alpha_2-1
- Upstream update to 4.0.0-alpha-2

* Mon Nov 07 2016 Ding-Yi Chen <dchen@redhat.com> - 4.0.0_alpha_1-3
- Test with EAP7
- Requires eap7-wildfly.

* Wed Oct 19 2016 Ding-Yi Chen <dchen@redhat.com> - 3.9.6-1
- Upstream update to 3.9.6

* Tue Aug 30 2016 Ding-Yi Chen <dchen@redhat.com> - 3.9.5-1
- Upstream update to 3.9.5

* Mon Aug 29 2016 Ding-Yi Chen <dchen@redhat.com> - 3.9.4-1
- Upstream update to 3.9.4

* Wed Jul 27 2016 Ding-Yi Chen <dchen@redhat.com> - 3.9.3-1
- Upstream update to 3.9.3

* Mon Jul 04 2016 Ding-Yi Chen <dchen@redhat.com> - 3.9.1-1
- Upstream update to 3.9.1

* Thu Jun 09 2016 Ding-Yi Chen <dchen@redhat.com> - 3.9.0-1
- Upstream update to 3.9.0

* Tue Apr 26 2016 Ding-Yi Chen <dchen@redhat.com> - 3.8.4-1
- Upstream update to 3.8.4

* Thu Mar 31 2016 Ding-Yi Chen <dchen@redhat.com> - 3.8.3-4
- context-root become '/'

* Mon Mar 28 2016 Ding-Yi Chen <dchen@redhat.com> - 3.8.3-1
- Upstream update to 3.8.3

* Tue Jan 05 2016 Ding-Yi Chen <dchen@redhat.com> - 3.8.2-1
- Upstream update to 3.8.2
- BuildRequires and Requires java-1.8.0

* Tue Dec 15 2015 Ding-Yi Chen <dchen@redhat.com> - 3.8.1-1
- Upstream update to 3.8.1
- BuildRequires and Requires java-1.8.0

* Fri Dec 11 2015 Ding-Yi Chen <dchen@redhat.com> - 3.8.0-1
- Upstream update to 3.8.0

* Thu Oct 15 2015 Ding-Yi Chen <dchen@redhat.com> - 3.7.3-6
- Upstream update to 3.7.3

* Thu Sep 10 2015 Ding-Yi Chen <dchen@redhat.com> - 3.7.2-1
- Upstream update to 3.7.2

* Thu Aug 06 2015 Ding-Yi Chen <dchen@redhat.com> - 3.7.1-7
- Use the auto generate release number.

* Tue Jul 21 2015 Ding-Yi Chen <dchen@redhat.com> - 3.7.1-4
- Add back the zanata-war-*.war to zanata.war symlink

* Tue Jul 21 2015 Ding-Yi Chen <dchen@redhat.com> - 3.7.1-3
- Upstream update to 3.7.1
- Remove the zanata-war-*.war to zanata.war symlink

* Mon Jul 13 2015 Sean Flanigan <sflaniga@redhat.com> - 3.7.0-3
- Requires: jbossas-standalone >= 7.5.0 (EAP 6.4.0)
- Set owner for /usr/share/webapps/zanata

* Mon Jul 06 2015 Ding-Yi Chen <dchen@redhat.com> - 3.7.0-2
- Remove BuildRequires: make

* Fri Jul 03 2015 Ding-Yi Chen <dchen@redhat.com> - 3.7.0-1
- Upstream update to 3.7.0

* Thu Apr 02 2015 Ding-Yi Chen <dchen@redhat.com> - 3.6.2-1
- Upstream update to 3.6.2

* Tue Mar 31 2015 Ding-Yi Chen <dchen@redhat.com> - 3.6.1-1
- Upstream update to 3.6.1
- Fix the changelog

* Wed Feb 25 2015 Ding-Yi Chen <dchen@redhat.com> - 3.6.0-2
- Fix the changelog
- Requires: jbossas-standalone>=7.4.0

* Tue Feb 24 2015 Ding-Yi Chen <dchen@redhat.com> - 3.6.0-1
- Upstream update to 3.6.0

* Fri Feb 28 2014 Sean Flanigan <sflaniga@redhat.com> - 3.3.2-2
- Update Requires: jbossas to >= 7.3.0
* Fri Feb 28 2014 Sean Flanigan <sflaniga@redhat.com> - 3.3.1-2
- Update Requires: jbossas to 7.3.0 (EAP 6.2.0)
* Fri Sep 20 2013 Sean Flanigan <sflaniga@redhat.com> - 3.0.3-4
- Remove BuildRequires: jbossas-standalone
- Replace chown with %files %attr
* Fri Sep 20 2013 Sean Flanigan <sflaniga@redhat.com> - 3.0.3-3
- Add BuildRequires: jbossas-standalone
* Fri Sep 20 2013 Sean Flanigan <sflaniga@redhat.com> - 3.0.3-2
- Require: jbossas-standalone and set owner for /var/lib/zanata to "jboss"
* Mon Oct 29 2012 Sean Flanigan <sflaniga@redhat.com> - 2.0.0-2
- Update zanata-war symlink now that there is a single war
* Thu May 31 2012 Sean Flanigan <sflaniga@redhat.com> - 1.6.0-1
- Add /var/lib/zanata/stats directory
* Fri Apr 13 2012 Sean Flanigan <sflaniga@redhat.com> - 1.5.0-3
- Do explode war files (Fedora packaging guidelines)
- In case of signer problems, build with -Dmavensign.expand.skip='*' -Dmavensign.sign.skip='*'
* Mon Mar 5 2012 Sean Flanigan <sflaniga@redhat.com> - 1.5.0-2
- Don't explode war files (preserve code signatures)
* Fri Mar 2 2012 Sean Flanigan <sflaniga@redhat.com> - 1.5.0-1
- Update for Zanata 1.5.0 (multiple war files)
* Fri Nov 25 2011 Graeme Gillies <ggillies@redhat.com> - 1.4.3-alpha-2
- Initial version

# Introduction

PostSRSd is used to mitigate strict SPF settings (like done by Red Hat) in order to allow forwarding from a custom domain. For example myname@example.com aliases to example@redhat.com would fail. PostSRSd rewrites addresses similarly to mailing-lists softwares to allow this.

You can find more information on these sites:
* https://github.com/roehling/postsrsd
* https://en.wikipedia.org/wiki/Sender_Rewriting_Scheme

# Reason for Packaging

This package is missing on Fedora and CentOS.

Review: https://bugzilla.redhat.com/show_bug.cgi?id=1471056


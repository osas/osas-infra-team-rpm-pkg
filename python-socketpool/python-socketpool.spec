%if 0%{?fedora} || 0%{?rhel} > 7
%global with_python3 1
%endif

%global pypi_name socketpool

Name:           python-%{pypi_name}
Version:        0.5.3
Release:        14%{?dist}
Summary:        A simple Python socket pool

License:        Public Domain or MIT
URL:            https://github.com/benoitc/%{pypi_name}
Source0:        https://pypi.python.org/packages/source/s/%{pypi_name}/%{pypi_name}-%{version}.tar.gz

# For py3 compat when building in koji.
# https://github.com/benoitc/socketpool/pull/36
Patch0:         python-socketpool-setup-encoding.patch

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python2-setuptools

%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%endif

%description
Socket pool is a simple socket pool that supports multiple factories and
backends. It can easily be used by gevent, eventlet or any other library.


%package -n python2-socketpool
Summary:        A simple Python socket pool
%{?python_provide:%python_provide python2-socketpool}

Requires:       python2

%description -n python2-socketpool
Socket pool is a simple socket pool that supports multiple factories and
backends. It can easily be used by gevent, eventlet or any other library.

%if 0%{?with_python3}
%package -n python3-socketpool
Summary:        A simple Python socket pool
%{?python_provide:%python_provide python3-socketpool}

Requires:       python3

%description -n python3-socketpool
Socket pool is a simple socket pool that supports multiple factories and
backends. It can easily be used by gevent, eventlet or any other library.
%endif

%prep
%setup -q -n %{pypi_name}-%{version}
rm -rf %{pypi_name}.egg-info

%patch0 -p1

%build
%py2_build
%if 0%{?with_python3}
%py3_build
%endif

%install
%py2_install
%if 0%{?with_python3}
%py3_install
%endif

# Erroneously installed stuff.  Remove it!
rm %{buildroot}/usr/socketpool/LICENSE
rm %{buildroot}/usr/socketpool/MANIFEST.in
rm %{buildroot}/usr/socketpool/NOTICE
rm %{buildroot}/usr/socketpool/README.rst
rm %{buildroot}/usr/socketpool/THANKS
rm %{buildroot}/usr/socketpool/UNLICENSE

%files -n python2-socketpool
%doc README.rst THANKS UNLICENSE NOTICE examples/
%license LICENSE
%{python2_sitelib}/%{pypi_name}*

%if 0%{?with_python3}
%files -n python3-socketpool
%doc README.rst THANKS UNLICENSE NOTICE examples/
%license LICENSE
%{python3_sitelib}/%{pypi_name}*
%endif

%changelog
* Sat Feb 02 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 0.5.3-12
- Rebuilt for Python 3.7

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Fri Sep 29 2017 Troy Dawson <tdawson@redhat.com> - 0.5.3-10
- Cleanup spec file conditionals

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.5.3-7
- Rebuild for Python 3.6

* Tue Apr 05 2016 Ralph Bean <rbean@redhat.com> - 0.5.3-6
- Define with_python3 macro.  Silly me...
- Apply patch to fix char encoding issue in setup.py.

* Mon Mar 28 2016 Ralph Bean <rbean@redhat.com> - 0.5.3-5
- Added a python3 subpackage.

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Sep 26 2013 Bohuslav Kabrda <bkabrda@redhat.com> - 0.5.3-1
- Update to 0.5.3.

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Mon Nov 05 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 0.5.2-1
- Update to 0.5.2.

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Mar 16 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 0.3.0-2
- Added the %%{pypi_name} macro for the package name.
- Removed bundled egg-info directory.

* Fri Mar 16 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 0.3.0-1
- Initial package.


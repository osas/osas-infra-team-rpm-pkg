%if 0%{?fedora} || 0%{?rhel} > 7
%global with_python3 1
%endif

Name:           python-http-parser
Version:        0.8.3
Release:        19%{?dist}
Summary:        HTTP request/response parser for Python

License:        MIT
URL:            https://github.com/benoitc/http-parser/
Source0:        https://pypi.python.org/packages/source/h/http-parser/http-parser-%{version}.tar.gz
# already upstream: https://github.com/benoitc/http-parser/commit/6c7430649a8d1263181c000f5b5c6480c6efce6e
Patch0:         fix-build-on-py3.patch

BuildRequires:  gcc
BuildRequires:  python2-devel
BuildRequires:  python2-setuptools

%filter_provides_in %{python_sitearch}/http_parser/parser.so
%filter_setup

%global _description\
HTTP request/response parser for Python compatible with Python 2.x\
(>=2.5.4), Python 3 and Pypy. If possible a C parser based on\
http-parser_ from Ryan Dahl will be used.

%description %_description

%package -n python2-http-parser
Summary: %summary
%{?python_provide:%python_provide python2-http-parser}

%description -n python2-http-parser %_description

%if 0%{?with_python3}
%package -n python3-http-parser
Summary:        HTTP request/response parser for Python 3

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%description -n python3-http-parser
HTTP request/response parser for Python compatible with Python 2.x
(>=2.5.4), Python 3 and Pypy. If possible a C parser based on
http-parser_ from Ryan Dahl will be used.
This package contains Python 3 build of http-parser.
%endif

%prep
%setup -q -n http-parser-%{version}
%patch0 -p1


%build
%py2_build
%if 0%{?with_python3}
%py3_build
%endif


%install
%py2_install
%if 0%{?with_python3}
%py3_install
%endif


%files -n python2-http-parser
%doc LICENSE README.rst
%{python2_sitearch}/http_parser*


%if 0%{?with_python3}
%files -n python3-http-parser
%doc LICENSE README.rst
%{python3_sitearch}/http_parser*
%endif


%changelog
* Sat Feb 02 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.3-19
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.3-18
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Wed Jul 11 2018 Slavek Kabrda <bkabrda@redhat.com> - 0.8.3-17
- Fix building with Python 3.7

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 0.8.3-16
- Rebuilt for Python 3.7

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.3-15
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jan 25 2018 Iryna Shcherbina <ishcherb@redhat.com> - 0.8.3-14
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Sat Aug 19 2017 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 0.8.3-13
- Python 2 binary package renamed to python2-http-parser
  See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.3-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.3-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Jul 07 2017 Igor Gnatenko <ignatenko@redhat.com> - 0.8.3-10
- Rebuild due to bug in RPM (RHBZ #1468476)

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.3-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.8.3-8
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.3-7
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sun Nov 15 2015 Slavek Kabrda <bkabrda@redhat.com> - 0.8.3-5
- Introduced python3 subpackage

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Sep 26 2013 Bohuslav Kabrda <bkabrda@redhat.com> - 0.8.3-1
- Update to 0.8.3.

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.12-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.12-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Mon Nov 05 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 0.7.12-1
- Update to 0.7.12.

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Mar 15 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 0.7.5-2
- Removed unneeded rm -rf %%{buildroot}.
- Placed all files under the %%{python_sitearch}.
- Filtered provide of the .so file.

* Thu Mar 15 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 0.7.5-1
- Initial package.

